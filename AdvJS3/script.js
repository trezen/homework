"use strict";

class Employee {
  constructor({ age, name, salary }) {
    this.age = age;
    this.name = name;
    this.salary = salary;
  }
  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }
  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }
  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }

  // setAge(value) {
  //   this._age = value < 0 ? 0 : value;
  // }
  // showName() {
  //   console.log(this.name);
  // }
}

class Programmer extends Employee {
  constructor({ age, name, salary, lang }) {
    super({ age, name, salary });
    this.lang = lang;
  }
  set salary(value) {
    this._salary = (value * 5).toString();
  }
  get salary() {
    return this._salary;
  }
}

const prog1 = new Programmer({
  age: "35",
  name: "Ivan",
  salary: "1000",
  lang: ["Eng", "Ru"],
});
console.log(prog1);

const prog2 = new Programmer({
  age: "27",
  name: "Petro",
  salary: "2000",
  lang: ["Eng", "Ru", "Ua"],
});
console.log(prog2);

const prog3 = new Programmer({
  age: "23",
  name: "John",
  salary: "1400",
  lang: ["Eng", "Klingon"],
});
console.log(prog3);
