"use strict";

function createNewUser() {
  let firstName;
  do {
    firstName = prompt("введите имя");
  } while (firstName === null || firstName === "");
  let lastName;
  do {
    lastName = prompt("введите фамилию");
  } while (lastName === null || lastName === "");

  let birth;
  do {
    birth = prompt("введите день рождения в формате дд.мм.гг", "22.10.2000");
  } while (birth === null || birth === "");
  let newUser = {
    name: firstName,
    last: lastName,
    birthday: birth,
    getLogin: function () {
      return this.name[0].toLowerCase() + this.last.toLowerCase();
    },

    getAge: function () {
      birth = birth.split(".");
      let birthTimeStamp = new Date(birth[2], birth[1] - 1, birth[0]);
      let currentTimeStamp = new Date();
      let difference = currentTimeStamp - birthTimeStamp;
      let currentAge = Math.floor(difference / 31557600000); ///1000*60*60*24*365.25
      return currentAge;
    },
    getPassword: function () {
      return this.name[0].toUpperCase() + this.last.toLowerCase() + birth[2];
    },
  };
  return;
}

createNewUser();

let user = createNewUser();
console.log("Your login is " + user.getLogin());
console.log("Your age is " + user.getAge());
