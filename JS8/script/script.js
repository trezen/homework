"use strict";

let btn = document.createElement("button");
const input = document.getElementById("input");
let mar = document.getElementById("price_input");

input.addEventListener("focus", greenFrame);
function greenFrame(e) {
  e.target.style.cssText = "border: 2px solid green;";
}

input.addEventListener("blur", inputCheck);
function inputCheck() {
  if (input.value < 0 || input.value === "" || isNaN(input.value)) {
    input.style.cssText = "border: 2px solid red";
    let negativeValue = document.getElementById("negative");
    if (negativeValue !== null) negativeValue.remove(); /// это чтоб при повторном вооде негативного числа не спаунились спаны в каждой итерации
    negativeValue = document.createElement("span");
    negativeValue.textContent = `Please enter correct price`;
    negativeValue.style.cssText = "margin-left: 100px";
    document.body.append(negativeValue);
    negativeValue.id = "negative";
    mar.style.cssText = "margin: 100px;"; // здесь и в 29й строчке меиняется цсс для самого инпута, чтобы спан, который появляется выше него, не сдвигал сам инпут ниже
    return;
  }
  let negativeValue = document.getElementById("negative");
  if (negativeValue !== null) negativeValue.remove();

  mar.style.cssText =
    "margin: 0; position: absolute;  left: 100px;  top: 100px;";
  btn.textContent = "×";
  btn.style.cssText = "margin-left: 20px";
  document.body.prepend(btn);
  let priceValue = input.value;

  let curPrice = document.createElement("span");
  curPrice.textContent = `Текущая цена: ${priceValue}`;
  curPrice.style.cssText = "margin-left: 100px;";
  document.body.prepend(curPrice);
  input.style.cssText = "color: green;";

  btn.addEventListener("mousedown", clearValue);

  function clearValue() {
    curPrice.remove();
    btn.remove();
    input.value = "";
  }

  input.addEventListener("blur", inputCheck2);
  function inputCheck2() {
    curPrice.remove();
  }
}
