"use strict";

let menupanel = $(".menu__items");

for (let i = 0; i < menupanel.length; i++) {
  menupanel[i].addEventListener("mousedown", function click(e) {
    $("#" + e.target.id).click(function () {
      $([document.documentElement, document.body]).animate(
        {
          scrollTop: $("#" + e.target.id + "_section").offset().top,
        },
        1000
      );
    });
  });
}

let mybutton = document.getElementById("myBtn");

$(document).on("scroll", function () {
  scrollFunction();
});

function scrollFunction() {
  if (
    document.body.scrollTop > window.innerHeight ||
    document.documentElement.scrollTop > window.innerHeight
  ) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

function topFunction() {
  $("html, body").animate({ scrollTop: 0 }, 600);
}

$("#hide").on("click", function () {
  $("#clients_section").slideToggle();
});
