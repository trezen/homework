"use strict";

// let n1 = prompt("введите число");
// const j = 5;
// if (n1 < 0) console.log("Sorry, no numbers");
// for (let x = 0; x <= n1; x++) {
//   if (x % j == 0) {
//     console.log(x);
//   } //написал через отдельный конст, чтоб его можно было менять на любое другое число
// }

// дополнительные 1. проверка на целое
// const j = 5;
// let n1;
// do {
//   n1 = prompt("введите целое число");
// } while (n1 === null || isNaN(n1) || n1 === "" || n1 % 1 !== 0);
// {
//   for (let x = 0; x < n1; x++) {
//     if (x % j == 0) {
//       console.log(x);
//     } else if (n1 < j) console.log("Sorry, no numbers");
//   }
// }

// дополнительные 2 простые
let n;
let m;
do {
  n = prompt("первое число");
} while (n === null || isNaN(n) || n === "" || n % 1 !== 0);

do {
  m = prompt("второе число, побольше");
} while (m === null || isNaN(m) || m === "" || m % 1 !== 0 || m <= n);
{
  for (let i = n; i <= m; i++) {
    let flag = 0;
    for (let j = 2; j < i; j++) {
      // вместо корня можно просто написать < i, но тогда оно будет проверять вообще все числа, и на больших значениях это долго, поэтому достаточно проверить на корень
      if (i % j == 0) {
        flag = 1;
        break;
      }
    }
    if (i > 1 && flag == 0) {
      console.log(i);
    }
  }
}
