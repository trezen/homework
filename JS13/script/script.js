"use strict";

if (localStorage.getItem("theme") == "black") {
  themeChanger();
  // document.body.classList.add("change-bg");
  // document.getElementsByClassName("menu")[0].classList.toggle("change-buttons");
  // сначала тут были вот эти две строки но потом я понял что можно сразу функцию из кнопки вызывать, если условие выполняется
}

let switchTheme = document.createElement("button");
switchTheme.innerHTML = "сменить тему";
switchTheme.id = "btn";
switchTheme.className = "change_style_button";

document.body.prepend(switchTheme);

document.getElementById("btn").addEventListener("click", themeChanger);

function themeChanger() {
  if (document.body.classList.contains("change-bg")) {
    localStorage.setItem("theme", "white");
    document.body.classList.remove("change-bg");
    document
      .getElementsByClassName("menu")[0]
      .classList.toggle("change-buttons");
  } else {
    document.body.classList.add("change-bg");
    document
      .getElementsByClassName("menu")[0]
      .classList.toggle("change-buttons");
    localStorage.setItem("theme", "black");
  }
}
