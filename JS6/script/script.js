"use strict";

function filterBy(massiv, filter) {
  let newArr = massiv.filter(function (type) {
    return typeof type != filter;
  });
  return newArr;
}

console.log(
  filterBy(["hello", "world", 23, "23", null, undefined, NaN], "object")
);
