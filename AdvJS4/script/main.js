"use strict";

const URI = "https://swapi.dev/api/films/";

async function getMovies() {
  const response = await fetch(URI).then((response) => response.json());
  const data = await response;
  return data;
}
getMovies()
  .then((data) => {
    const div = document.createElement("div");
    document.body.prepend(div);

    div.style.display = "flex";
    div.style.width = "100%";
    div.style.flexWrap = "wrap";

    data.results.forEach((e, index) => {
      const movie = document.createElement("p");
      const epId = document.createElement("span");
      const crawl = document.createElement("p");
      movie.style.margin = "20px";
      div.append(movie);
      const border = document.getElementsByTagName("p")[index * 2];
      border.style.borderStyle = "solid";
      border.style.padding = "10px";
      border.style.width = "400px";
      movie.textContent = "Movie Title: " + e.title;
      movie.id = index + 1 + "_movie";
      movie.append(epId);
      epId.textContent = ", Episode: " + e.episode_id;
      movie.append(crawl);
      crawl.textContent = "Synopsis: " + e.opening_crawl.substring(0, 200);
    });
  })
  .catch((e) => {
    console.log(e);
  });

getMovies().then((data) => {
  data.results.forEach(({ characters, title }, x) => {
    let mmm = document.getElementById(x + 1 + "_movie");
    const loader = document.createElement("div");
    loader.innerHTML = '<div class="loader"></div>';
    mmm.append(loader);
    if (mmm.id == x + 1 + "_movie") {
      const xxx = document.createElement("ul");
      xxx.textContent = "Characters:";
      characters.map(async function name(x) {
        const response = await fetch(x).then((response) => response.json());
        const xxxli = document.createElement("li");
        xxxli.textContent += response.name;
        loader.remove();
        mmm.append(xxx);
        xxx.append(xxxli);
      });
    }
  });
});
