function showList(array, parent) {
  let arraynew = array.map(function (el) {
    let item = document.createElement("li");
    item.textContent = el;
    return item;
  });
  const fragment = document.createElement("ul");
  fragment.append(...arraynew);
  if (parent == "" || parent == null) {
    document.body.append(fragment);
  } else {
    parent.append(fragment);
  }
}
const books = [1, 22, 3];

showList(books, parentP); // для пробы сделал два id: parentP и parentDiv.
