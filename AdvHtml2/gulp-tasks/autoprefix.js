const gulp = require("gulp");
const autoprefixer = require("gulp-autoprefixer");

gulp.task("pref", function () {
  return gulp
    .src("./src/css_pre/style.css")
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 2 versions"],
        cascade: false,
      })
    )
    .pipe(gulp.dest("./src/css_pre/"));
});

exports.pref = "pref";
