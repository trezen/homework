const gulp = require("gulp");
const imagemin = require("gulp-imagemin");

exports.imgmin = () =>
  gulp.src("src/img/*").pipe(imagemin()).pipe(gulp.dest("dist/img"));
