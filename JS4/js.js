"use strict";

function createNewUser() {
  let firstName;
  do {
    firstName = prompt("введите имя");
  } while (firstName === null || firstName === "");
  let lastName;
  do {
    lastName = prompt("введите фамилию");
  } while (lastName === null || lastName === "");

  let newUser = {
    name: firstName,
    last: lastName,
    getLogin: function () {
      return this.name[0].toLowerCase() + this.last.toLowerCase();
    },
  };
  return newUser;
}

let user = createNewUser();
console.log("Your login is " + user.getLogin());
