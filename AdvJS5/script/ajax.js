const URI = "https://api.ipify.org/?format=json";
const URI2 = "http://ip-api.com/json/";

const findMe = async function findMe() {
  response = await fetch(URI);
  let address = await response.json();

  response2 = await fetch(URI2 + address.ip);
  info = await response2.json();
  let { country, city, regionName, countryCode, timezone } = info;
  let stats = {
    timezone: timezone,

    "country code": countryCode,
    country: country,
    region: regionName,
    city: city,
    "favourite drink": "coffee",
    time: "is running out. Run.",
  };
  console.log(stats);
  for (let e in stats) {
    const element = document.createElement("div");
    const p = document.createElement("p");
    p.innerHTML = `> Your ${e} is ${stats[e]} <`;
    document.getElementById("ip-place").append(element);
    p.classList = ["mt-2 mb-0"];
    element.append(p);
  }
};
