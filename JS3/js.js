"use strict";

let num1;
let num2;
let operation;

do {
  num1 = prompt("первое число");
} while (num1 === null || isNaN(num1) || num1 === "" || num1 % 1 !== 0);

do {
  num2 = prompt("второе число");
} while (num2 === null || isNaN(num2) || num2 === "" || num2 % 1 !== 0);

do {
  operation = prompt("операция");
} while (
  operation === "" ||
  operation === null ||
  (operation !== "/" &&
    operation !== "*" &&
    operation !== "+" &&
    operation !== "-")
);

function calc(num1, num2, operation) {
  switch (operation) {
    case "*":
      console.log(num1 * num2);
      break;
    case "+":
      console.log(+num1 + +num2);
      break;
    case "-":
      console.log(num1 - num2);
      break;
    case "/":
      console.log(num1 / num2);
      break;
    default:
      break;
  }
}

calc(num1, num2, operation);

// Необязательное. Полный код, нужно закоментить все что выше. Добавлены проверки и подставление значений по умолчанию

// let operation;
// let num1 = prompt("введите число 1");
// if (num1 === null || isNaN(num1) || num1 === "" || num1 % 1 !== 0)
//   do {
//     num1 = prompt("Вы не ввели число, введите число 1", `${num1}`); // подставляем значение первого промпта в следующие итерации
//   } while (num1 === null || isNaN(num1) || num1 === "" || num1 % 1 !== 0);

// let num2 = prompt("введите число 2");
// if (num2 === null || isNaN(num2) || num2 === "" || num2 % 1 !== 0)
//   do {
//     num2 = prompt("Вы не ввели число, введите число 2");
//   } while (num2 === null || isNaN(num2) || num2 === "" || num2 % 1 !== 0);

// do {
//   operation = prompt("операция");
// } while (
//   operation === "" ||
//   operation === null ||
//   (operation !== "/" &&
//     operation !== "*" &&
//     operation !== "+" &&
//     operation !== "-")
// );

// function calc(num1, num2, operation) {
//   switch (operation) {
//     case "*":
//       console.log(num1 * num2);
//       break;
//     case "+":
//       console.log(+num1 + +num2);
//       break;
//     case "-":
//       console.log(num1 - num2);
//       break;
//     case "/":
//       console.log(num1 / num2);
//       break;
//     default:
//       break;
//   }
// }

// calc(num1, num2, operation);
