"use strict";

document.addEventListener("keydown", function (event) {
  let findkey = document.getElementsByClassName("btn");
  let arr = Array.from(findkey);
  function keyCheck() {
    //сначала я написал такой форич для каждого ивента. потом подумал что раз они повторяются,
    // можно их отдельно вынести в функцию. По идее как-то еще можно сократить, но не могу понять как
    arr.forEach((element) => {
      element.style.cssText = "color: none ";
      if (element.textContent == event.key.toUpperCase()) {
        element.style.cssText = "background-color: blue";
      }
    });
  }

  if (
    // event.code == ("KeyZ" || "KeyN" || "KeyO" || "KeyL" || "KeyE" || "KeyS") // не работает ?
    event.code == "KeyZ" ||
    event.code == "KeyN" ||
    event.code == "KeyO" ||
    event.code == "KeyL" ||
    event.code == "KeyE" ||
    event.code == "KeyS"
  ) {
    keyCheck();
  }

  if (event.code == "Enter") {
    // для ентера приходится отдельный писать потому что апперкейс не срабатывает
    arr.forEach((element) => {
      element.style.cssText = "color: none ";
      if (element.textContent == event.key)
        element.style.cssText = "background-color: blue";
    });
  }
});

/////////////// переписал после подсказки 
// "use strict";
// function keyCheck() {
//   document.addEventListener("keydown", function (event) {
//     let findkey = document.getElementsByClassName("btn");
//     let arr = Array.from(findkey);
//    arr.forEach((element) => {
//       element.style.cssText = "color: none ";
//       console.log(event.key);
//       if (
//         event.key === element.textContent.toLowerCase() ||
//         event.key === element.textContent
//       ) {
//         element.style.cssText = "background-color: blue";
//       }
//     });
//   });
// }
// keyCheck();
