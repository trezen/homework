"use strict";
let inputElem = document.getElementsByClassName("tabs-title");

for (let i = 0; i < inputElem.length; i++) {
  inputElem[i].addEventListener(
    "mousedown",
    function () {
      let activeTab = document.querySelector(".active");
      activeTab.classList.remove("active");
      this.classList.add("active");

      let visibleText = document.querySelectorAll(".visible");
      visibleText.forEach(function (el) {
        let active = document.querySelector(".visible");
        active.classList.remove("visible");
        active.classList.add("toggletab");
      });

      let chapter = this.classList[1].charAt(3);
      let toggleText = document.querySelector("#list").children[chapter - 1];
      toggleText.className = "visible";
    },
    false
  );
}
