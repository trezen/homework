"use strict";

let slideIndex = 0;
let timer = setInterval(slideshow, 3000);
slideshow();

function slideshow() {
  let img = document.getElementsByClassName("image-to-show");
  for (let i = 0; i < img.length; i++) {
    img[i].style.display = "none";
  }
  if (slideIndex == img.length) {
    slideIndex = 0;
  }
  img[slideIndex].style.display = "block";
  slideIndex++;
}

let stop = document.createElement("button");
stop.innerHTML = "Мой любимый мидер";
stop.id = "btn1";

document.body.append(stop);
document.getElementById("btn1").addEventListener("click", stopTimer);
function stopTimer() {
  clearTimeout(timer);

  let resume = document.createElement("button");
  resume.innerHTML = "Выбрать другого";
  resume.id = "btn2";
  if (document.getElementById("btn2") == null) {
    document.body.append(resume);
    document.getElementById("btn2").addEventListener("click", stop);
  }

  function stop() {
    timer = setInterval(slideshow, 3000);
    resume.remove();
  }
}
